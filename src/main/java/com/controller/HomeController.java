package com.controller;

import com.business.redis.service.RedisService;
import com.business.user.model.User;
import com.config.RedisConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by guanlei on 2018/1/17.
 */
@RestController
public class HomeController {

    private final static Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private User user;
    @Autowired
    private RedisService redisServiceImpl;
    /**
     * 这个方法返回出去的是Hello Spring Boot!字符串，要想渲染HTML页面应将@RestController注解改成@Controller
     * 这样springboot就会找到指定的jsp来渲染页面。
     */
    @RequestMapping(value = "/home",produces = "text/plain;charset=UTF-8")
    public String index(){
//        System.out.println("user:"+user);
//        RedisConfig redisConfig = new RedisConfig();
//        KeyGenerator keyGenerator = redisConfig.keyGenerator();
//        boolean isSuccess = redisServiceImpl.set("user", this.user);
//        Object getUser = redisServiceImpl.get("user");
        logger.info("首页请求...");
        return "Hello Spring Boot!";
    }
}
