package com;

import com.config.Interceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 整个项目的入口类(这个类是创建工程后自动生成的，命名规则是：artifactId+Application
 *
 * @SpringBootApplication注解: 整个Spring Boot的核心注解，它是一个组合注解(@Configuration、@EnableAutoConfiguration和@ComponentScan)，
 * 它的目的就是开启Spring Boot的自动配置
 *
 * @ServletComponentScan 项目启动的时候会先加载监听器(只调用初始化方法contextInitialized) -> 过滤器(只调用初始化方法init)
 *
 * @author lei.guan
 */
@SpringBootApplication
@ServletComponentScan   //扫描监听器、过滤器
//@ImportResource(locations = {"classpath:application.properties"}) //加载配置的xml
public class SpringbootApplication extends WebMvcConfigurerAdapter {

	private final static Logger logger = LoggerFactory.getLogger(SpringbootApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringbootApplication.class, args);
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new Interceptor());
		logger.info("添加自己的拦截器成功！");
	}
}
