package com.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by guanlei on 2018/1/23.
 * 监听器为Servlet中的接口，通常配置在web.xml文件中；
 * 项目启动时会第一调用监听器的contextInitialized()方法，可用来加载一些配置文件
 */
@WebListener
public class Listener implements ServletContextListener {

    private final static Logger logger = LoggerFactory.getLogger(Listener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("监听器初始化成功！");
        Properties properties = new Properties();
        InputStream inputStream = Listener.class.getResourceAsStream("/config.ini");
        try {
            properties.load(inputStream);
            logger.info("加载config.ini文件成功！");
        } catch (Exception e) {
            logger.info("加载config.ini文件失败！"+e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.info("监听器销毁！");
    }
}
