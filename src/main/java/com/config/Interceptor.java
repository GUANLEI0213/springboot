package com.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by guanlei on 2018/1/23.
 * 拦截器为spring中的接口，通常配置在spring.xml中
 */
public class Interceptor implements HandlerInterceptor {

    private final static Logger logger = LoggerFactory.getLogger(Interceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        logger.info("拦截器-在请求之前调用,也就是Controller方法调用之前");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
        logger.info("拦截器-在请求处理之后渲染视图之前调用，也就是Controller方法调用之后");
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        logger.info("拦截器-在渲染视图之后整个请求处理之后调用，也就是在DispatcherServlet渲染了对应的视图之后调用，主要是用于资源清理工作");
    }
}
