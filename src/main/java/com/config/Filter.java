package com.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by guanlei on 2018/1/23.
 * 过滤器为Servlet中的接口，通常配置在web.xml文件中；
 * 项目启动时会调用过滤器的init()方法，被过滤到的请求会调用doFilter()方法具体处理；
 * 注意：过滤掉的请求不在经过Controller，会走下面的doFilter方法，此Filter只过滤了/hh一个请求
 */
@WebFilter(filterName = "filter",urlPatterns = "/hh")
public class Filter implements javax.servlet.Filter {

    private final static Logger logger = LoggerFactory.getLogger(Filter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("过滤器初始化成功！");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("过滤器处理逻辑"+servletRequest.getRequestDispatcher(""));
    }

    @Override
    public void destroy() {
        logger.info("过滤器销毁");
    }
}
