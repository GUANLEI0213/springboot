package com.business.user.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * Created by lei.guan on 2017/10/17.
 */

/**
 * 使用@Component注解来保证待会该pojo可以被spring容器所注入，即注入bean，放在IOC容器中
 * 使用@ConfigurationProperties(prefix = “user”)注解来获取配置文件中前缀为user的配置，prefix指的是前缀
 * 注意redis的相关model需要实现序列化
 */
@Component
//@ConfigurationProperties(prefix = "user")
public class User implements Serializable {
    @Value("${user.name1}")
    private String name;
    @Value("${user.age}")
    private int age;
    @Value("${user.sex}")
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}
