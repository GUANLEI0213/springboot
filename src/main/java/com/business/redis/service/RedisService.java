package com.business.redis.service;

import java.util.List;
import java.util.Set;

/**
 * Created by lei.guan on 2018/1/11.
 */
public interface RedisService {

    /**
     * 写入缓存
     */
    boolean set(final String key,Object value);

    /**
     * 写入缓存，设置缓存失效时间
     */
    boolean set(final String key,Object value,Long expireTime);

    /**
     * 批量删除key
     */
    void removeKeys(final String key);

    /**
     *  批量删除value
     */
    void remove(final String...keys);

    /**
     *  非批量删除key对应的value
     */
    void remove(final String key);

    /**
     * 判断缓存中是否存在对应的value
     */
    boolean exists(final String key);

    /**
     * 读取缓存
     */
    Object get(final String key);

    /**
     * 哈希 添加
     */
    void  hmSet(String key,Object hashKey,Object value);

    /**
     * 哈希获取数据
     */
    Object hmGet(String key,Object hashKey);

    /**
     * 列表添加
     */
    void lPush(String key,Object value);

    /**
     * 列表获取
     */
    List<Object> lRange(String key,long l,long ll);

    /**
     * 集合添加
     */
    void add(String key,Object value);

    /**
     * 集合获取
     */
    Set<Object> setMembers(String key);

    /**
     * 有序集合添加
     */
    void zAdd(String key,Object value,double scoure);

    /**
     * 有序集合添加
     */
    Set<Object> rangeByScore(String key,double scoure,double scoure1);
}
