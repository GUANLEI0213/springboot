package com.business.redis.service.impl;

import com.business.redis.service.RedisService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created by guanlei on 2018/1/11.
 */
@Service(value = "redisServiceImpl")
public class RedisServiceImpl implements RedisService {

    private final static Logger logger = Logger.getLogger(RedisServiceImpl.class);
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 写入缓存
     *
     * @param key
     * @param value
     */
    @Override
    public boolean set(String key, Object value) {
        boolean result = false;
        try{
            ValueOperations<Serializable,Object> valueOperations = redisTemplate.opsForValue();
            valueOperations.set(key,value);
            result = true;
        }catch (Exception e){
            logger.error(e);
        }
        return result;
    }

    /**
     * 写入缓存，设置缓存失效时间
     *
     * @param key
     * @param value
     * @param expireTime
     */
    @Override
    public boolean set(String key, Object value, Long expireTime) {
        boolean result = false;
        try{
            ValueOperations valueOperations = redisTemplate.opsForValue();
            valueOperations.set(key,value);
            redisTemplate.expire(key,expireTime, TimeUnit.SECONDS);
            result = true;
        }catch (Exception e){
            logger.error(e);
        }
        return result;
    }

    /**
     * 批量删除key
     *
     * @param key
     */
    @Override
    public void removeKeys(String key) {
        Set keys = redisTemplate.keys(key);
        if (keys.size() > 0)
            redisTemplate.delete(keys);
    }

    /**
     * 批量删除value
     *
     * @param keys
     */
    @Override
    public void remove(String... keys) {
        for (String key : keys){
            remove(key);
        }
    }

    /**
     * 非批量删除key对应的value
     *
     * @param key
     */
    @Override
    public void remove(String key) {
        if (!exists(key)) return;
        redisTemplate.delete(key);
    }

    /**
     * 判断缓存中是否存在对应的value
     *
     * @param key
     */
    @Override
    public boolean exists(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 读取缓存
     *
     * @param key
     */
    @Override
    public Object get(String key) {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        return valueOperations.get(key);
    }

    /**
     * 哈希 添加
     *
     * @param key
     * @param hashKey
     * @param value
     */
    @Override
    public void hmSet(String key, Object hashKey, Object value) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put(key,hashKey,value);
    }

    /**
     * 哈希获取数据
     *
     * @param key
     * @param hashKey
     */
    @Override
    public Object hmGet(String key, Object hashKey) {
        HashOperations hashOperations = redisTemplate.opsForHash();
        return hashOperations.get(key,hashKey);
    }

    /**
     * 列表添加
     *
     * @param key
     * @param value
     */
    @Override
    public void lPush(String key, Object value) {
        ListOperations listOperations = redisTemplate.opsForList();
        listOperations.leftPush(key,value);
    }

    /**
     * 列表获取
     *
     * @param key
     * @param l
     * @param ll
     */
    @Override
    public List<Object> lRange(String key, long l, long ll) {
        ListOperations listOperations = redisTemplate.opsForList();
        return listOperations.range(key,l,ll);
    }

    /**
     * 集合添加
     *
     * @param key
     * @param value
     */
    @Override
    public void add(String key, Object value) {
        SetOperations setOperations = redisTemplate.opsForSet();
        setOperations.add(key,value);
    }

    /**
     * 集合获取
     *
     * @param key
     */
    @Override
    public Set<Object> setMembers(String key) {
        SetOperations setOperations = redisTemplate.opsForSet();
        return setOperations.members(key);
    }

    /**
     * 有序集合添加
     *
     * @param key
     * @param value
     * @param scoure
     */
    @Override
    public void zAdd(String key, Object value, double scoure) {
        ZSetOperations<String,Object> zSet = redisTemplate.opsForZSet();
        zSet.add(key,value,scoure);
    }

    /**
     * 有序集合添加
     *
     * @param key
     * @param scoure
     * @param scoure1
     */
    @Override
    public Set<Object> rangeByScore(String key, double scoure, double scoure1) {
        ZSetOperations<String,Object> zSet = redisTemplate.opsForZSet();
        return zSet.rangeByScore(key, scoure, scoure1);
    }
}
