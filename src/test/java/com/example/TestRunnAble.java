package com.example;

import com.model.LambdaExample;

import java.util.Arrays;
import java.util.List;

/**
 * Created by lei.guan on 2017/10/23.
 */
public class TestRunnAble {

    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Before JAVA8");
            }
        }).start();

        new Thread(()->System.out.println("In JAVA8,Lambda")).start();

        List<Integer> integers = Arrays.asList(2, 1, 4, 3,6,5);
        integers.forEach(i -> System.out.println(i));

        /**
         * Lambda表达式：
         *
         * (参数列表)->一个表达式或语句块
         *
         * Lambda表达式用来简化创建接口，不能用来创建类或者抽象类，并且只能用来简化仅包含一个public方法的接口的创建
         *
         */
//      (x, y) -> x + y;
//      (x, y) -> {return x+y;};

        LambdaExample lambdaExample = (x, y) -> x + y;

        LambdaExample lambdaExample1 = (x, y) -> {return x + y;};

        System.out.println("Lambda表达式求和："+lambdaExample);

    }

    

}
