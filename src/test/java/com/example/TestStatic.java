package com.example;

import com.model.MyClass;

/**
 * Created by guanlei on 2018/1/22.
 */
/**
 * static关键字用法总结：
 * 1.static代码块可以出现在类中的任何地方，但就是不能出现在某个方法中,并且程序是按照代码块的先后顺序执行的；
 * 2.static代码块在类加载的时候会首先加载首先执行，且只会加载一次执行一次，所以可以提高系统的性能，减少内存占用
 * 3.注意类加载的时候若该类继承了父类会先加载父类,父类内容全部加载完成后再加载子类
 */
public class TestStatic {

    public static void main(String[] args) {
        new MyClass();
    }
}
