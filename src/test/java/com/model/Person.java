package com.model;

/**
 * Created by guanlei on 2018/1/22.
 */
public class Person {

    // 3)
    static {
        System.out.println("Person static");
    }

    public Person(String value){
        System.out.println("Person:"+value);
    }
}
