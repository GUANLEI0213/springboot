package com.model;

/**
 * Created by guanlei on 2018/1/22.
 */
public class MyClass extends Student {

    // 5)
    Person person = new Person("myClass");

    // 2)
    static {
        System.out.println("MyClass static");
    }

    // 6)
    public MyClass(){
        System.out.println("MyClass constructor");
    }

}
