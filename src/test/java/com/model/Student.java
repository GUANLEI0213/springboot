package com.model;

/**
 * Created by guanlei on 2018/1/22.
 */
public class Student {

    // 4)
    Person person = new Person("student");

    // 1)
    static {
        System.out.println("Student static");
    }

}
